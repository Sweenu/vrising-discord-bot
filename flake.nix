{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      poetry2nix,
    }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; })
        mkPoetryApplication
        mkPoetryEnv
        defaultPoetryOverrides
        ;
      p2n-overrides = defaultPoetryOverrides.extend (
        self: super: {
          discord = super.discord.overridePythonAttrs (old: {
            buildInputs = (old.buildInputs or [ ]) ++ [ super.setuptools ];
          });
        }
      );
    in
    {
      packages.x86_64-linux.vrising-discord-bot = mkPoetryApplication {
        projectDir = self;
        python = pkgs.python311;
        overrides = p2n-overrides;
      };
      packages.x86_64-linux.default = self.packages.x86_64-linux.vrising-discord-bot;

      devShells.x86_64-linux.default = pkgs.mkShellNoCC {
        packages = with pkgs; [
          (mkPoetryEnv {
            projectDir = self;
            python = pkgs.python311;
            overrides = p2n-overrides;
          })
          poetry
          python311
        ];
      };

      nixosModules.vrising-discord-bot = import ./modules/vrising-discord-bot.nix;
      nixosModules.default = self.nixosModules.vrising-discord-bot;

      overlays.default = _: _: { vrising-discord-bot = self.packages.x86_64-linux.vrising-discord-bot; };
    };
}
