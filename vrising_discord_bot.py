import json
import logging
import sys
import tomllib

import discord
import requests

from discord import app_commands

logger = logging.getLogger(__name__)

with open(sys.argv[1], "rb") as conffile:
    config = tomllib.load(conffile)

with open(config["portainer"]["access_key_file"]) as f:
    PORTAINER_ACCESS_KEY = f.readline().rstrip("\n")
PORTAINER_API_URL = f"{config['portainer']['base_url']}/api/endpoints/{str(config['portainer']['environment_id'])}/docker/containers"
CONTAINER_NAME = config["portainer"]["container_name"]

# Hardcode guild ID to have instant sync of commands
GUILD = discord.Object(id=config["discord"]["guild_id"])


def list_containers(**filters):
    response = requests.get(
        f"{PORTAINER_API_URL}/json", 
        headers={"X-API-Key": PORTAINER_ACCESS_KEY}, 
        params={"filters": json.dumps(filters)},
    )
    return response


def container_action(id: str, action: str):
    response = requests.post(f"{PORTAINER_API_URL}/{id}/{action}", headers={"X-API-Key": PORTAINER_ACCESS_KEY})
    return response


class DiscordClient(discord.Client):

    def __init__(self, *, intents: discord.Intents):
        super().__init__(intents=intents)
        self.tree = app_commands.CommandTree(self)

    async def setup_hook(self):
        self.tree.copy_global_to(guild=GUILD)
        await self.tree.sync(guild=GUILD)


intents = discord.Intents.default()
client = DiscordClient(intents=intents)

@client.event
async def on_ready():
    print(f'Logged in as {client.user} (ID: {client.user.id})')


@client.tree.command(description="Start the server")
async def start(interaction: discord.Interaction):
    try:
        response = list_containers(name=[CONTAINER_NAME], status=["created", "paused", "exited", "dead"])
        response.raise_for_status()
        container = response.json()[0]

        response = container_action(container["Id"], "start")
        response.raise_for_status()
        await interaction.response.send_message("Server starting...")
    except requests.exceptions.RequestException as e:
        logger.error(e)
        await interaction.response.send_message(f"API error: {response.status_code}")
    except IndexError:
        await interaction.response.send_message("Server is already running.")

        
@client.tree.command(description="Stop the server")
async def stop(interaction: discord.Interaction):
    try:
        response = list_containers(name=[CONTAINER_NAME], status=["running"])
        response.raise_for_status()
        container = response.json()[0]

        response = container_action(container["Id"], "stop")
        response.raise_for_status()
        await interaction.response.send_message("Server stopping...")
    except requests.exceptions.RequestException as e:
        logger.error(e)
        await interaction.response.send_message(f"API error: {response.status_code}")
    except IndexError:
        await interaction.response.send_message("Server is already stopped.")


def main():
    with open(config["discord"]["bot_token_file"]) as f:
        bot_token = f.readline().rstrip("\n")
    client.run(bot_token)


if __name__ == "__main__":
    main()
