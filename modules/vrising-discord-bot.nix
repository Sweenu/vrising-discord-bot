{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  cfg = config.services.vrising-discord-bot;
  format = pkgs.formats.toml { };
  configFile = format.generate "vrising-discord-bot.toml" cfg.settings;
in
{
  options.services.vrising-discord-bot = {
    enable = mkEnableOption "vrising-discord-bot";
    settings = mkOption {
      type = types.submodule { freeformType = format.type; };
      default = { };
      description = "Configuration for the bot and for the portainer API, example in config.toml.";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.vrising-discord-bot = {
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        ExecStart = "${pkgs.vrising-discord-bot}/bin/vrising-discord-bot ${configFile}";
      };
    };
  };
}
